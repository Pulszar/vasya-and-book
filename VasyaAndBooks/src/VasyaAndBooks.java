import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StreamTokenizer;

/**
 * This is solution written in java for the problem: Vasya and Book.
 * Original problem can be found here:
 * https://codeforces.com/problemset/problem/1073/B
 * @author skywalker
 *
 */
public class VasyaAndBooks
{

	public static void main(String[] args) throws IOException
	{
		BufferedReader bi = new BufferedReader(new InputStreamReader(System.in));
		String lineOne = bi.readLine();
		String lineTwo = bi.readLine();
		String lineThree = bi.readLine();
		bi.close();

		int n = Integer.parseInt(lineOne);

		int[] indexN = new int[n +1];
		int r=0;
		
		for (String numStr: lineTwo.split("\\s"))
		{
			int bn = Integer.parseInt(numStr);
			indexN[bn] = r;
			r++;
		}

		boolean printZeros = false;
		int countOfPrinted = 0;
		int c = 0;
		int biggestIndex =0;
		int sum = 0;
		int previous = -1;
		int i=0;
		for (String numStr: lineThree.split("\\s"))
		{
			int a = Integer.parseInt(numStr);

			int indexOfNumber = indexN[a] + 1;
			if(indexOfNumber > biggestIndex)
			{
				biggestIndex = indexOfNumber;
				int current;
				sum = indexOfNumber;
				if(previous == -1)
				{
					previous = sum;
					current = sum;
				}
				else
				{
					current = sum - previous;
					previous = sum;
				}
				c = current;
			}
			else
			{
				c = 0;
			}
			System.out.print(c + " ");
			
			if(indexN[a] == n -1)
			{
				printZeros = true;
				countOfPrinted = i +1;
				break;
			}
			i++;
		}

		if (printZeros)
		{
			while (n > countOfPrinted)
			{
				countOfPrinted++;
				System.out.print("0 ");
			}
		}

	}

}
